:-(clause(powoduje(_),_); consult('db.pl')).

%% ---------------------------------------------------------------------------------
%% ---------------------------------------------------------------------------------
%% ---------------------------------------------------------------------------------
%% ************************************** OUT **************************************

wypisz_chorobe(NozwaChoroby) :-
  write("Twoja choroba to: "), 
  nl,
  write("***************************"), nl,
  write(NozwaChoroby), nl,
  write("***************************"), nl.

%% ---------------------------------------------------------------------------------

wypisz_objawy(Objawy) :-
  write("Twoje objawy: "), write(Objawy).

%% ---------------------------------------------------------------------------------

brak_pasujacych_chorob :- 
  write("\nBrak chor�b pasujacych do danych objaw�w.").

%% ---------------------------------------------------------------------------------

nie_zdiagnozowano :-
  write("Nie zdiagnozowano jeszcze nic. \n").

%% ---------------------------------------------------------------------------------

czy_jest_stringiem(Object) :-
  forall(member(X, Object), number(X)). 

%% ---------------------------------------------------------------------------------

masz_tyle_chorob(LiczbaChorob) :-
  format("Masz ~d mozliwych chor�b. ~n", LiczbaChorob).

%% ---------------------------------------------------------------------------------

seperator :-
  write("-------------"),nl.

%% ---------------------------------------------------------------------------------

czy_masz_objaw(PierwszyUnikalnyPotencjalnyObjaw) :-
  write("Czy masz nastepujacy objaw: "), write(PierwszyUnikalnyPotencjalnyObjaw), write("?"), nl, nl.

%% ---------------------------------------------------------------------------------

wypisz_choroby(Choroby) :-
  write("Mozesz miec nastepujace choroby: "), nl,
  printlist(Choroby).


%% ---------------------------------------------------------------------------------
%% ---------------------------------------------------------------------------------
%% ---------------------------------------------------------------------------------
%% *********************************** MAIN CORE ***********************************

powodujeX(Choroba, []).
powodujeX(Choroba, [Glowa|Ogon]) :-
  powoduje(Choroba, Glowa), powodujeX(Choroba, Ogon).


nazywa_sie(choroba(X), Nazwa) :-
  Nazwa = X.

diagnozuj() :-
  diagnozuj([], []).

diagnozuj(Objawy, NieObjawy) :-
  wypisz_objawy(Objawy), nl,
  wypisz_objawy(NieObjawy), nl,
  findall(Choroba, powodujeX(Choroba, Objawy), Choroby), %%.% znajduje wszystkie choroby ktore sa wywolywane przez zadane objawy
  length(Objawy, LiczbaObjawow),
  sprawdz_dane(Choroby, LiczbaObjawow),
  zapytaj_o_chorobe(Choroby, Objawy, NowyObjaw, NieObjawy, NowyNieObjaw),
  append([NowyObjaw], Objawy, NoweObjawy), %%.% laczy liste objawow z nowym objawem (lub null jesli nie wykryto)
  append([NowyNieObjaw], NieObjawy, NoweNieObjawy), %%.% laczy liste nie objawow z nowym objawem (lub null jesli nie wykryto)
  usun_duplikaty(NoweObjawy, UnikalneNoweObjawy),
  usun_duplikaty(NoweNieObjawy, UnikalneNoweNieObjawy),

  subtract(UnikalneNoweObjawy, ['null'], UnikalneNoweObjawyOstateczne), %%.% odfiltrowuje wartosci null dla nieznalezionych  elementow
  subtract(UnikalneNoweNieObjawy, ['null'], UnikalneNoweNieObjawyOstataczne), %%.% odfiltrowuje wartosci null dla nieznalezionych  elementow

  diagnozuj(UnikalneNoweObjawyOstateczne, UnikalneNoweNieObjawyOstataczne).  %%.% ponowna diagnostyka dla nowych parametrow

sprawdz_dane(Choroby, LiczbaObjawow) :- %%.% sprawdza czy dla danych da sie prowadzic diagnostyke
  Choroby = [], LiczbaObjawow > 0 ->
    brak_pasujacych_chorob, false;
  [PierwszaChoroba|ResztaChorob] = Choroby, %%.% rozdziela choroby na dwa zmienne
  sprawdzChoroby(PierwszaChoroba, ResztaChorob). %%.% dane pozwalaja na dalsze dzialania, dalsze sprawdzenia poprawnosci

sprawdzChoroby(PierwszaChoroba, ResztaChorob) :-
  czy_dokladnie_jedna(PierwszaChoroba, ResztaChorob) ->
    nl, nazywa_sie(PierwszaChoroba, NozwaChoroby), wypisz_chorobe(NozwaChoroby), nl, false;
  PierwszaChoroba = [] ->
    nie_zdiagnozowano; %%.% obecne dane nie pozwalaja na jednoznaczna diagnostyke, informacja
    length([PierwszaChoroba | ResztaChorob], LiczbaChorob), masz_tyle_chorob(LiczbaChorob).

czy_dokladnie_jedna(PierwszaChoroba, ResztaChorob) :-
  czy_jest_stringiem(PierwszaChoroba), PierwszaChoroba \= [], ResztaChorob = [].

zapytaj_o_chorobe(Choroby, Objawy, NowyObjaw, NieObjawy, NowyNieObjaw) :-
  seperator,
  (\+ Objawy == [] -> 
    findall(Objaw, ((member(Choroba, Choroby)), powoduje(Choroba, Objaw)), WszystkiePotencjalneObjawy), nl, %%.% znajduje wszystkie objawy dla danych chorob
    append(Objawy, NieObjawy, WykluczoneObjawyDoPredykcji), %%.% laczy juz wykryte objawy z wykluczonymi do jednej tabeli
    subtract(WszystkiePotencjalneObjawy, WykluczoneObjawyDoPredykcji, PotencjalneObjawy), %%.% odejmuje od objawow dla chorob wykluczone objawy (zeby nie pytac ponownie o te same objawy)
    usun_duplikaty(PotencjalneObjawy, UnikalnePotencjalneObjawy),
    sprawdz_pytania(UnikalnePotencjalneObjawy, Choroby),
    [PierwszyUnikalnyPotencjalnyObjaw|_] = UnikalnePotencjalneObjawy, %%.% wybranie pierwszego objawu z listy do pytania
    czy_masz_objaw(PierwszyUnikalnyPotencjalnyObjaw),
    read(Odpowiedz),
    (Odpowiedz == 'tak' ->
      NowyObjaw = PierwszyUnikalnyPotencjalnyObjaw, NowyNieObjaw = null; %%.% odpowiedz brzmi tak, objaw trafia, nieobjaw dostaje null
      NowyObjaw = null, NowyNieObjaw = PierwszyUnikalnyPotencjalnyObjaw) %%.% odpowiedz nie, na odwrot
  ;

  zapytaj_o_pierwszy_objaw(NowyObjaw, NowyNieObjaw)

  ),

  seperator.

sprawdz_pytania(UnikalnePotencjalneObjawy, Choroby) :-
  UnikalnePotencjalneObjawy == [] ->
    wypisz_choroby(Choroby), false;
    true.


zapytaj_o_pierwszy_objaw(NowyObjaw, NowyNieObjaw) :-
  findall(Objaw, powoduje(_, Objaw), Objawy),

  usun_duplikaty(Objawy, ObjawyUnikalne),
  znajdz_objaw(ObjawyUnikalne, NowyObjaw),

  %% write("Podaj nowy objaw: "),nl,
  %% read(NowyObjaw), nl, nl,
  NowyNieObjaw = 'null'.


znajdz_objaw([PierwszyObjaw|ResztaObjawow], NowyObjaw) :-
  write("Czy masz objaw "),
  write(PierwszyObjaw),
  write("?"), nl,

  read(Odpowiedz),
  Odpowiedz == 'tak' ->
    NowyObjaw = PierwszyObjaw;
    znajdz_objaw(ResztaObjawow, NowyObjaw).




%% ---------------------------------------------------------------------------------
%% ---------------------------------------------------------------------------------
%% ---------------------------------------------------------------------------------
%% ******************************* ADDITIONAL SOURCE *******************************





% An empty list is a usun_duplikaty.
usun_duplikaty([], []).

% Put the head in the result,
% remove all occurrences of the head from the tail,
% make a usun_duplikaty out of that.
usun_duplikaty([H|T], [H|T1]) :- 
    remv(H, T, T2),
    usun_duplikaty(T2, T1).

% Removing anything from an empty list yields an empty list.
remv(_, [], []).

% If the head is the element we want to remove,
% do not keep the head and
% remove the element from the tail to get the new list.
remv(X, [X|T], T1) :- remv(X, T, T1).

% If the head is NOT the element we want to remove,
% keep the head and
% remove the element from the tail to get the new tail.
remv(X, [H|T], [H|T1]) :-
    X \= H,
    remv(X, T, T1).

printlist([]).

printlist([X|List]) :-
    write(X),nl,
    printlist(List).

start :- 
  nl, nl, write('Witaj w gabinecie! '), nl, 
  diagnozuj; nl, write("------ Koniec diagnozy ------"),nl,
  write("czy chcesz kontynuowac? tak/nie"), nl,
  read(Odpowiedz),
  write(Odpowiedz), nl,
  (Odpowiedz == 'tak') ->
  nl, write("kontunuujemy"), start;
  write("koniec"), nl, halt.

exit :-
  halt.

