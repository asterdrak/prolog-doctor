# README #

Projekt w języku Prolog symulujący diagnostykę prostych chorób na podstawie bazy wiedzy składającej się z predykatów: jaka choroba powoduje jaki objaw.

### Sterowanie ###

Program zadaje pytania i wyświetla odpowiedzi. Użytkownik potrzebuje jedynie odpowiadać "tak." lub "nie." i czytać odpowiedzi na ekranie.

### Podstawy działania ###

Program pozwala zdiagnozować choroby ze swojej bazy wiedzy, zadając pytania o kolejne objawy i automatycznie dopasowując kryteria w czasie działania programu.
W pierwszej kolejności użytkownik jest wypytywany o objawy do pierwszego trafienia - w tym momencie program ogranicza liczbę możliwych chorób do tego objawu (inne choroby i ich objawy już nie są brane pod uwagę). Następnie na podstawie tych chorób wylicza możliwe objawy i zadaje pytanie o pierwszy z nich. Jeśli objaw występuje zostaje on dodany do listy objawów wg której tworzona jest lista chorób, jeśli objaw nie występuje zostaje dodany do listy która zawsze będzie odejmowana od listy przewidywanych objawów - pytanie nie zostanie zadane drugi raz. 

### Statystyka ###

Program pozwala na wyznaczanie choroby na podstawie objawów w sposób efektywny. We właściwej części programu pytania się nie powtarzają a listy chorób i objawów są sukcesywnie minimalizowane. Większą realną wydajność może przynieść przemieszanie kolejności danych w bazie - program po kolei przepyta o wszystkie objawy danej choroby jeśli znajdują się obok siebie. Program powinien poprawnie odpowiadać na wszysytkie możliwe sytuacje (brak diagnozy, diagnoza wielokrotna, diagnoza na podstawie minimalnej wymaganej wiedzy). Zasadniczym problemem programu jest możliwa powtarzalność pytań w pierwszej i drugiej fazie - nie ma połączenia pomiędzy pytaniami wstępnej selekcji a późniejszej eliminacji chorób.
